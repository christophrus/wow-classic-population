const handleUpload = require('./handleUpload');
const listRealms = require('./listRealms');
const getCharacterStats = require('./getCharacterStats');
const getTimeStats = require('./getTimeStats');
const getQuickStats = require('./getQuickStats');

module.exports = {
  handleUpload,
  listRealms,
  getCharacterStats,
  getTimeStats,
  getQuickStats
};
